package cz.muni.ics.kypo.elasticsearch.data.enums;

public enum TrainingType {
    /**
     * Adaptive training type.
     */
    ADAPTIVE,
    /**
     * Linear training type.
     */
    LINEAR
}
